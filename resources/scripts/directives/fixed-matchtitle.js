'use strict';

app.directive('fixedMatchtitle', ['$window', function ($window) {
    var $win = angular.element($window);

    return {
        link: function (scope, element, attrs) {
            angular.element(document).ready(function () {
                var fixedClass = attrs['fixedMatchtitle'],
                    offsetTop = element[0].offsetHeight + 15;

                $win.on('scroll', function (e) {
                    if (window.pageYOffset >= offsetTop) {
                        element.addClass(fixedClass);
                    } else {
                        element.removeClass(fixedClass);
                    }
                });
            });
        }
    };
}]);