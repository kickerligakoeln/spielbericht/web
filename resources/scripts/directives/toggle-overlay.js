'use strict';

app.directive('toggleOverlay', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                element.toggleClass('-in');

                jQuery(attrs.toggleOverlay).toggleClass('-in');
                jQuery('body').toggleClass('-open-overlay');
            });
        }
    };
});