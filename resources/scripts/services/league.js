'use strict';

app.factory('league', ['$http', 'app', function ($http, app) {
    var league = {};

    /**
     * get all active leagues from
     * /wp-json/kkl/v1/leagues?active=49&fields=id,code,name&embed=currentseason
     *
     * @param fn
     */
    league.getActiveLeagues = function (fn) {
        $http({
            url: app.apiRoute('/getActiveLeagues'),
            method: 'GET'
        })
            .then(
                function successCallback(response) {
                    fn(response);
                }
            );
    };

    /**
     * get table / ranks from
     * /wp-json/kkl/v1/seasons/66/ranking
     *
     * @param seasonId
     * @param fn
     */
    league.getRanksBySeasonId = function (seasonId, fn) {
        if (seasonId) {
            $http({
                url: app.apiRoute('/getRanksBySeasonId'),
                method: 'GET',
                params: {
                    season_id: seasonId
                }
            })
                .then(
                    function successCallback(response) {
                        fn(response);
                    }
                );
        }
    };

    return league;
}]);
