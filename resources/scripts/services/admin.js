'use strict';

app.factory('admin', ['$http', '$$cookieReader', 'app', function ($http, $$cookieReader, app) {
    var admin = {};

    /**
     * return callback function if authentification is true
     * @param fn
     */
    admin.getAdminAuth = function (fn) {
        $http({
            method: 'GET',
            url: app.apiRoute('/getAdminAuth'),
            headers: {
                'Admin': $$cookieReader()['ADMIN_AUTH']
            }
        })
            .then(
                function successCallback(response) {
                    fn(response.data);
                }, function errorCallback(response) {
                    fn(response);
                }
            );

    };

    return admin;
}]);