'use strict';

app.factory('scoresheet', ['$http', '$$cookieReader', 'app', function ($http, $$cookieReader, app) {
    var scoresheet = {};

    var getSearchParam = function (val) {
        var result = '',
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === val) {
                    result = decodeURIComponent(tmp[1]);
                }
            });
        return result;
    };


    /**
     * return showcase of scoresheet
     * without mail addys
     *
     * @param srcScoresheet
     * @returns {any}
     */
    var logScoresheet = function (srcScoresheet) {
        var consoleScoresheet = JSON.parse(JSON.stringify(srcScoresheet));

        if (consoleScoresheet && consoleScoresheet.team_home) {
            delete consoleScoresheet.team_home.mail;
            delete consoleScoresheet.team_guest.mail;
        }

        return consoleScoresheet;
    };

    /**
     * get matchId parameter of URL
     * @returns {string}
     */
    scoresheet.getIdFromUrl = function () {
        return getSearchParam('match');
    };


    /**
     * save scoresheet in localStorage and
     * try to save in database
     * in any case -> continue!
     * @param values
     * @param fn
     */
    scoresheet.save = function (values, fn) {
        localStorage.setItem('scoresheet', JSON.stringify(values));

        $http.post(app.apiRoute('/saveState'), values, {
            headers: {
                'Editor': $$cookieReader()['EDITOR_AUTH']
            }
        })
            .then(
                function successCallback(response) {
                    fn(response);
                }, function errorCallback(response) {
                    fn(response);
                }
            );
    };


    /**
     * remove scoresheet from localStorage
     */
    scoresheet.reset = function () {
        localStorage.clear('scoresheet');
    };


    /**
     * save scoresheet and continue with sending mail
     * @param values
     * @param fn
     */
    scoresheet.finalize = function (values, fn) {
        scoresheet.save(values, function () {
            scoresheet.sendMail(values, function (message) {
                delete values.team_home.mail;
                delete values.team_guest.mail;

                scoresheet.save(values, function () {
                    scoresheet.removeValidation(values.matchid, function () {
                    });
                });

                fn(message);
            });
        });
    };


    /**
     * send e-mail with scoresheet data
     * @param sheet
     * @param fn
     */
    scoresheet.sendMail = function (sheet, fn) {
        $http.post(app.apiRoute('/sendMail'), sheet)
            .then(
                function successCallback(response) {
                    fn(response.data);
                }
            );
    };


    /**
     * get scoresheet from localStorage
     * @returns {*}
     */
    scoresheet.get = function () {
        try {
            var scoresheet = JSON.parse(localStorage.getItem('scoresheet'));
            console.log('Scoresheet:', logScoresheet(scoresheet));

            return scoresheet;
        } catch (e) {
            var errorMsg = {
                error: {
                    login: new Date(),
                    message: 'no localStorage available',
                    connection: navigator.connection,
                    userAgent: navigator.userAgent,
                    vendor: navigator.vendor
                }
            };

            console.error(errorMsg);
            return errorMsg;
        }
    };


    /**
     * get scoresheet by matchId from database
     * @param id
     * @param fn
     */
    scoresheet.getById = function (id, fn) {
        $http({
            url: app.apiRoute('/getScoresheetById'),
            method: 'GET',
            params: {
                match: id
            }
        })
            .then(
                function successCallback(response) {
                    if (response.data['matchInfo']) {
                        console.log('Scoresheet:', logScoresheet(response.data));

                        fn(response.data);
                    } else {
                        fn(false);
                    }
                }
            );
    };


    /**
     * remove scoresheet by matchId from database
     * @param id
     * @param fn
     */
    scoresheet.removeById = function (id, fn) {
        $http({
            url: app.apiRoute('/removeScoresheetById'),
            method: 'GET',
            headers: {
                'Admin': $$cookieReader()['ADMIN_AUTH']
            },
            params: {
                match: id
            }
        })
            .then(
                function successCallback(response) {
                    if (response.data) {
                        fn(response.data);
                    }
                }
            );
    };


    /**
     * get all scoresheets from database
     * @param gameType
     * @param season
     * @param fn
     */
    scoresheet.getAllForSeason = function (gameType, season, fn) {
        $http({
            url: app.apiRoute('/getAllScoresheetsForSeason?game_type=' + gameType + '&season=' + season),
            method: 'GET',
            headers: {
                'Admin': $$cookieReader()['ADMIN_AUTH']
            }
        })
            .then(
                function successCallback(response) {
                    if (response.data) {
                        fn(response.data);
                    }
                }
            );
    };


    /**
     * get all Live scoresheets from database
     * @param options
     * @param fn
     */
    scoresheet.getAllLiveGames = function (options, fn) {
        var params = '?';
        angular.forEach(options, function (option, key) {
            params += key + '=' + option + '&';
        });

        $http({
            url: app.apiRoute('/getAllLiveGames' + params),
            method: 'GET'
        })
            .then(
                function successCallback(response) {
                    if (response.data) {
                        fn(response.data);
                    }
                }
            );
    };


    /**
     * set validation entry
     * @param id
     * @param fn
     */
    scoresheet.setValidation = function (id, fn) {
        console.log('setValidation');

        $http({
            url: app.apiRoute('/setValidation'),
            method: 'GET',
            params: {
                match: id
            }
        })
            .then(
                function successCallback(response) {
                    if (response.data) {
                        document.cookie = 'EDITOR_AUTH=' + response.data + '; path=/';
                        fn(response.data);
                    }
                }
            );
    };


    /**
     * remove Validation entry
     * @param id
     * @param fn
     */
    scoresheet.removeValidation = function (id, fn) {
        $http({
            url: app.apiRoute('/removeValidation'),
            method: 'GET',
            params: {
                match: id
            }
        })
            .then(
                function successCallback(response) {
                    if (response.data) {
                        fn(response.data);
                    }
                }
            );
    };


    return scoresheet;
}]);
