'use strict';

app.factory('match', ['$http', 'app', function ($http, app) {
    var match = {};

    /**
     * Get upcoming matches from
     * /wp-json/kkl/v1/gamedays/{gamedayId}/matches?embed=homeTeam,awayTeam,location,gameDay
     *
     * @param gamedayId
     * @param fn
     */
    match.getUpcomingMatchesByGamedayId = function (gamedayId, fn) {
        if (gamedayId) {
            $http({
                url: app.apiRoute('/getUpcomingMatchesByGamedayId'),
                method: 'POST',
                params: {
                    gamedayId: gamedayId
                }
            })
                .then(
                    function successCallback(response) {
                        fn(response);
                    }
                );
        }
    };

    return match;
}]);
