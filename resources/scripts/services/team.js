'use strict';

app.factory('team', ['$http', 'app', function ($http, app) {
    var team = {};

    /**
     * get Team Details from
     * /wp-json/kkl/v1/teams/{teamId}/
     *
     * @param teamId
     * @param fn
     */
    team.getDetails = function (teamId, fn) {
        if (teamId) {
            $http({
                url: app.apiRoute('/getTeamDetails'),
                method: 'GET',
                params: {
                    team_id: teamId
                }
            })
                .then(
                    function successCallback(response) {
                        fn(response);
                    }
                );
        }
    };

    return team;
}]);
