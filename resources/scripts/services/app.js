'use strict';

app.factory('app', [function () {
    var app = {};

    /**
     *
     * @param setting
     * @param item
     * @returns {string}
     */
    app.globalSettings = function (setting, item) {
        if (setting === 'app') {
            return (window.app_settings[item]) ? window.app_settings[item] : '';
        }
    };

    app.internalRoute = function (target) {
        target = target || '';
        window.location = this.globalSettings('app', 'base_path') + '/' + target;
    };

    app.apiRoute = function (target) {
        target = target || '';
        return this.globalSettings('app', 'api_path') + target;
    };

    return app;
}]);