'use strict';

app.factory('statistics', ['$http', 'app', function ($http, app) {
    var statistics = {};


    /**
     *
     * @param gameType
     * @param season
     * @param fn
     */
    statistics.getAggregatedStats = function (gameType, season, fn) {
        $http({
            url: app.apiRoute('/getAggregatedStatistics?game_type=' + gameType + '&season=' + season),
            method: 'GET'
        })
            .then(
                function successCallback(response) {
                    if (response.data) {
                        fn(response.data);
                    }
                }
            );
    };

    return statistics;
}]);