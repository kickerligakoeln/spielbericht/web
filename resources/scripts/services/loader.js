'use strict';

app.factory('loader', ['$http', function ($http) {
    var loader = {
        $loader: $('#loader'),
        $loaderText: $('#loader p')
    };

    loader.fadeIn = function () {
        loader.$loader.addClass('-in');
    };

    loader.fadeOut = function () {
        loader.$loaderText.html('');
        loader.$loader.removeClass('-in');
    };

    loader.showFunnyText = function (name) {
        this.fadeIn();
        //get the text
        var foaas = [
            {'path': 'donut', 'name': true},
            {'path': 'outside', 'name': true},
            {'path': 'cool'},
            {'path': 'what'},
            {'path': 'awesome'},
            {'path': 'think', 'name': true},
            {'path': 'single'},
            {'path': 'look', 'name': true},
            {'path': 'fascinating'}
        ];
        var random = Math.floor(Math.random() * foaas.length);
        if (foaas[random].name) {
            if (name) {
                name = '/' + name;
            } else {
                name = '/Captain';
            }
        } else {
            name = '';
        }
        $http({
            url: "https://www.foaas.com/" + foaas[random].path + name + "/KKL",
            method: 'GET'
        })
            .then(
                function successCallback(response) {
                    if (response && response.data && response.data.message) {
                        loader.updateText(response.data.message);
                    }
                }
            );
    };

    loader.showText = function (text) {
        this.fadeIn();
        loader.$loaderText.html(text);
    };

    loader.updateText = function (text) {
        loader.$loaderText.html(text);
    };


    return loader;
}]);
