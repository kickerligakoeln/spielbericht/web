'use strict';

app.factory('location', ['$http', 'app', function ($http, app) {
    var location = {};

    location.getAll = function (fn) {

        $http({
            url: app.apiRoute('/getAllLocations'),
            method: 'GET',
            params: {}
        })
            .then(
                function successCallback(response) {
                    fn(response);
                }
            );

    };

    return location;
}]);
