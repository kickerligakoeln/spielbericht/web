'use strict';

app.factory('websocket', [function () {
	'use strict';
	var websocket = {};

	switch (window.location.host) {
		case "spielbericht.kickerkarte.de":
			websocket.host = "wss://socket.kickerkarte.de";
			break;
		default:
			websocket.host = "ws://spiel.kickerligakoeln.de:63874";
			break;
	}

	/**
	 * @param matchId
	 */
	websocket.connect = function(matchId) {
		if (matchId) {
			websocket.connection = new WebSocket(websocket.host + "?matchid=" + parseInt(matchId));
		}
	};

	return {
		getWebsocket: function(matchId) {
			websocket.connect(matchId);
			return websocket.connection;
		}
	};
}]);
