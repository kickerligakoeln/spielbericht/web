'use strict';

app.factory('editor', ['app', function (app) {
    var editor = {};


    /**
     * @param admin
     * @returns {{login: Date, connection: WebSocket, userAgent: string, vendor: string, resolution: {width: number, height: number}, admin: *}}
     */
    editor.getReport = function (admin) {
        return {
            login: new Date(),
            connection: navigator.connection,
            userAgent: navigator.userAgent,
            vendor: navigator.vendor,
            resolution: {
                width: window.innerWidth,
                height: window.innerHeight
            },
            admin: admin
        }
    };


    /**
     * @param matchId
     */
    editor.logOut = function (matchId) {
        localStorage.removeItem('scoresheet');

        app.internalRoute('live?match=' + matchId);
    };

    return editor;
}]);