'use strict';

app.filter('activeGamePlayers', function () {
    return function (sheet) {
        if (typeof sheet === "undefined") return;
        for (var i = 0; i < sheet.matches.length; i++) {
            var match = sheet.matches[i];
            // let last match stand
            if (i === (sheet.matches.length - 1)) {
                return match.player.home_a + " / " + match.player.home_b;
            }
            // find first match with absolutely no score
            if (match.score[0].home > 0) continue;
            if (match.score[0].guest > 0) continue;
            if (match.score[1].home > 0) continue;
            if (match.score[1].guest > 0) continue;
            return match.player.home_a + " / " + match.player.home_b;
        }
    };
});
