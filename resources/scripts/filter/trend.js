'use strict';

app.filter('trend', function () {
    return function (score) {

        return score.home - score.guest || 0;
    };
});

app.filter('trendLabel', function () {
    return function (score) {
        var label = score.home - score.guest;

        if (label < 0) {
            label = label * (-1);
        }

        return label;
    };
});