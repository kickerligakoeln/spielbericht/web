'use strict';

app.filter('toString', function () {
    return function (val) {

        if (val === parseInt(val, 10)) {
            val = val.toString();
        }

        return val;
    };
});