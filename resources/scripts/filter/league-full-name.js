'use strict';

app.filter('leagueFullName', function() {
	return function(string) {
		var leagues = {
			"koeln1": "1. Liga",
			"koeln2a": "2. Liga A",
			"koeln2b": "2. Liga B",
			"koeln3a": "3. Liga A",
			"koeln3b": "3. Liga B",
			"koeln3c": "3. Liga C",
			"koeln4a": "4. Liga A",
			"koeln4b": "4. Liga B",
			"koeln4c": "4. Liga C",
			"koeln4d": "4. Liga D"
		};
		if (leagues[string]) {
			return leagues[string];
		} else {
			return '';
		}
	};
});

app.filter('leagueShortName', function() {
	return function(string) {
		var leagues = {
			"koeln1": "1",
			"koeln2a": "2a",
			"koeln2b": "2b",
			"koeln3a": "3a",
			"koeln3b": "3b",
			"koeln3c": "3c",
			"koeln4a": "4a",
			"koeln4b": "4b",
			"koeln4c": "4c",
			"koeln4d": "4d"
		};
		if (leagues[string]) {
			return leagues[string];
		} else {
			return '';
		}
	};
});