'use strict';

app.filter('roundNumber', function () {
	return function (number, fixed) {
		var roundNumber = '0';

		if (fixed === undefined) {
			fixed = 2;
		}

		if (number) {
			roundNumber = number.toFixed(fixed);
		}

		return roundNumber;
	};
});