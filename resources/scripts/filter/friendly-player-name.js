'use strict';

app.filter('friendlyPlayerName', function () {
    return function (obj) {
        var friendlyName = obj;

        if (friendlyName.length > 12) {
            var space = obj.search(" ");

            if (space !== -1) {
                friendlyName = obj.substr(0, space + 2) + '.';
            } else {
                friendlyName = friendlyName.substr(0, 9) + '...';
            }
        }

        return friendlyName;
    };
});


app.filter('friendlyPlayerName2', function () {
    return function (obj) {
        var friendlyName = obj;

        if (friendlyName.length > 10) {
            var space = obj.search(" ");

            if (space === -1) {
                friendlyName = friendlyName.substr(0, 9) + '...';
            }
        }

        return friendlyName;
    };
});