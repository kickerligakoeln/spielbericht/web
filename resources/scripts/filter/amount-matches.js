'use strict';

app.filter('amountMatches', function() {
    return function(obj) {
        var amountMatches = 0;
        for (var item in obj) {
            amountMatches = amountMatches + obj[item].length;
        }
        return String('0' + amountMatches).slice(-2);

    };
});