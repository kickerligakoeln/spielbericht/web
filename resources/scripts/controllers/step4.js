app.controller('step4Controller', ['$scope', 'app', 'scoresheet', 'loader', function ($scope, app, scoresheet, loader) {
    $scope.scoresheet = scoresheet.get();
    $scope.scoresheet.date['end'] = new Date();

    if (!$scope.scoresheet) {
        app.internalRoute();
    }
    $scope.sendForm = function () {
        loader.showFunnyText();
        scoresheet.finalize($scope.scoresheet, function (result) {
            loader.fadeOut();

            // TODO: check by getAdminAuto!
            switch (result) {
                case 'success':
                    $scope.showSuccess = true;
                    localStorage.removeItem('scoresheet');
                    break;

                default:
                    $scope.showSuccess = true;
            }

        });
    };
}]);