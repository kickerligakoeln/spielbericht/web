'use strict';

app.controller('matchOverviewController', ['$scope', 'scoresheet', 'loader', function ($scope, scoresheet, loader) {

    loader.updateText('Lade Spiele ...');
    loader.fadeIn();
    $scope.allMatches = {};


    /**
     * Toggle Seasons
     *
     * @param season
     */
    $scope.updateSeason = function (season) {
        $scope.season = parseInt(season);

        if (!$scope.allMatches[season]) {
            $scope.allMatches[season] = {};

            if (season === new Date().getFullYear()) {
                scoresheet.getAllLiveGames({}, function (allMatches) {
                    $scope.allMatches[season]['live'] = allMatches;
                });
            }

            scoresheet.getAllForSeason('gameday', season, function (allMatches) {
                $scope.allMatches[season]['gameday'] = allMatches;
            });

            scoresheet.getAllForSeason('custom', season, function (allMatches) {
                $scope.allMatches[season]['custom'] = allMatches;
            });

            scoresheet.getAllForSeason('cup', season, function (allMatches) {
                $scope.allMatches[season]['cup'] = allMatches;
            });

        }

        loader.fadeOut();
    };


    $scope.updateSeason(new Date().getFullYear());
}]);