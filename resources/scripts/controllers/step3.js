'use strict';

app.controller('step3Controller', ['$scope', 'app', 'editor', 'scoresheet', 'websocket', 'league', function ($scope, app, editor, scoresheet, websocket, league) {
    $scope.scoresheet = scoresheet.get();
    $scope.playerStats = [];
    $scope.manager = {
        code: "kkl-" + $scope.scoresheet.matchid
    };

    var webSocketConnection;
    if (!$scope.scoresheet || !$scope.scoresheet.matchid) {
        app.internalRoute();
    } else {
        webSocketConnection = websocket.getWebsocket($scope.scoresheet.matchid);
    }


    /**
     * reset and calculate goals / sets
     */
    var calculateTotalScore = function () {
        $scope.scoresheet.team_home['goals'] = 0;
        $scope.scoresheet.team_home['set'] = 0;
        $scope.scoresheet.team_guest['goals'] = 0;
        $scope.scoresheet.team_guest['set'] = 0;

        for (var h = 0; h < $scope.scoresheet['team_home'].player.length; h++) {
            var playerHome = $scope.scoresheet['team_home'].player[h];
            playerHome.score = {value: [0, 0], games: 0, goals: 0, sets: 0};
        }

        for (var g = 0; g < $scope.scoresheet['team_guest'].player.length; g++) {
            var playerGuest = $scope.scoresheet['team_guest'].player[g];
            playerGuest.score = {value: [0, 0], games: 0, goals: 0, sets: 0};
        }


        for (var i = 0; i < $scope.scoresheet.matches.length; i++) {
            if ($scope.scoresheet.matches[i].score) {

                for (var set in $scope.scoresheet.matches[i].score) {

                    for (var key in $scope.scoresheet.matches[i].score[set]) {
                        var value = parseInt($scope.scoresheet.matches[i].score[set][key]) || 0;

                        switch (key) {
                            case 'home':
                                $scope.scoresheet.team_home['goals'] += value;
                                if (value === 5) {
                                    $scope.scoresheet.team_home['set']++;
                                }
                                if ($scope.scoresheet.team_home['set'] === 11 && !$scope.scoresheet['winner']) {
                                    $scope.scoresheet['winner'] = {
                                        match: i + 1,
                                        team: 'team_home',
                                        set: parseFloat(key.split('_')[1])
                                    };
                                }
                                updatePlayerStats(i, 'home', value);

                                break;

                            case 'guest':
                                $scope.scoresheet.team_guest['goals'] += value;
                                if (value === 5) {
                                    $scope.scoresheet.team_guest['set']++;
                                }
                                if ($scope.scoresheet.team_guest['set'] === 11 && !$scope.scoresheet['winner']) {
                                    $scope.scoresheet['winner'] = {
                                        match: i + 1,
                                        team: 'team_guest',
                                        set: parseFloat(key.split('_')[1])
                                    };
                                }
                                updatePlayerStats(i, 'guest', value);

                                break;
                        }
                    }
                }
            }
        }
    };


    /**
     * Add up score in player data
     *
     * @param match
     * @param teamName
     * @param score
     */
    var updatePlayerStats = function (match, teamName, score) {
        var players = [
            $scope.scoresheet.matches[match].player[teamName + '_a'],
            $scope.scoresheet.matches[match].player[teamName + '_b']
        ];

        for (var i = 0; i < $scope.scoresheet['team_' + teamName].player.length; i++) {
            var player = $scope.scoresheet['team_' + teamName].player[i];

            if (player.name === players[0] || player.name === players[1]) {
                player.score.games++;
                player.score.goals += score;
                if (score === 5) {
                    player.score.sets++;
                }

                player.score.value[0] = player.score.sets * 100 / player.score.games;
                player.score.value[1] = 100 - player.score.value[0];
            }
        }
    };


    /**
     * update / save scoresheet
     * send to websocket
     */
    var updateScoreSheet = function () {
        $scope.scoresheet.date['update'] = new Date();
        calculateTotalScore();

        scoresheet.save($scope.scoresheet, function () {
            webSocketConnection.send(JSON.stringify($scope.scoresheet));
        });
    };
    updateScoreSheet();


    /**
     *  set a score, change set score of enemy
     * @param index
     * @param set
     * @param changed
     */
    $scope.changeValue = function (index, set, changed) {
        if (parseInt($scope.scoresheet.matches[index].score[set][changed]) < 5) {
            switch (changed) {
                case 'home':
                    $scope.scoresheet.matches[index].score[set]['guest'] = 5;
                    break;
                case 'guest':
                    $scope.scoresheet.matches[index].score[set]['home'] = 5;
                    break;
            }
        }
        updateScoreSheet();
    };


    /**
     * add / remove goals of specific match
     * @type {{addGoal: addGoal, subtractGoal: subtractGoal}}
     */
    $scope.singleScore = {
        addGoal: function (team) {
            var activeGame = $scope.activeMatch.match.game;
            var activeSet = $scope.activeMatch.match.set;

            if ($scope.scoresheet.matches[activeGame].score[activeSet]['home'] < 5 && $scope.scoresheet.matches[activeGame].score[activeSet]['guest'] < 5) {
                $scope.scoresheet.matches[activeGame].score[activeSet][team]++;
                updateScoreSheet();
            }
        },
        subtractGoal: function (team) {
            var activeGame = $scope.activeMatch.match.game;
            var activeSet = $scope.activeMatch.match.set;

            if ($scope.scoresheet.matches[activeGame].score[activeSet][team] > 0) {
                $scope.scoresheet.matches[activeGame].score[activeSet][team]--;
                updateScoreSheet();
            }
        }
    };


    /**
     * find first opened / running set in match-index
     * @param index
     * @returns {boolean}
     */
    var getRunningSet = function (index) {
        var runningSet = false;

        for (var key = 0; key < $scope.scoresheet.matches[index].score.length; key++) {
            var set = $scope.scoresheet.matches[index].score[key];

            if (set['home'] < 5 && set['guest'] < 5) {
                runningSet = key;
                break;
            }
        }

        return runningSet
    };


    /**
     * toggle active match
     * @type {{match: boolean, set: set, reset: reset}}
     */
    $scope.activeMatch = {
        match: false,
        set: function (index) {
            if (this.match.game === index) {
                this.reset();
            } else {
                if (getRunningSet(index) !== false) {
                    this.match = {
                        game: index,
                        set: getRunningSet(index)
                    };
                }
            }
        },
        reset: function () {
            this.match = false;
        }
    };


    /**
     *
     * @param isValid
     * @param event
     */
    $scope.submitForm = function (isValid, event) {
        updateScoreSheet();

        if (!isValid) {
            event.preventDefault();

            angular.forEach($scope.scoreboard.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        } else {
            app.internalRoute('step4');
        }
    };


    /**
     * get table of league
     */
    // TODO warte auf finalen endpoint
    var liveTable = function () {
            league.getRanksBySeasonId($scope.scoresheet.matchInfo.currentSeason, function (ranks) {
                if (ranks.data) {
                    $scope.rankings = ranks.data;
                }
            });
        };
    liveTable();


    /**
     * End of Editor Status
     * remove localStorage
     * @param e
     */
    $scope.managerLogout = function (e) {
        e.preventDefault();

        editor.logOut($scope.scoresheet.matchid);
    };
}]);