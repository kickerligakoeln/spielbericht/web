'use strict';

app.controller('indexController', ['$scope', 'scoresheet', function ($scope, scoresheet) {
    $scope.scoresheet = scoresheet.get();

    $scope.liveScoresheets = {};

    scoresheet.getAllLiveGames({}, function (liveScoresheets) {
        $scope.liveScoresheets = liveScoresheets;
    });

}]);