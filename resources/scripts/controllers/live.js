'use strict';

app.controller('liveController', ['$scope', 'app', 'scoresheet', 'websocket', 'editor', 'league', function ($scope, app, scoresheet, websocket, editor, league) {
    var webSocketConnection;
    if (!$scope.env.isLocal) {
        var leftTheTab = false;
        jQuery(window).blur(function () {
            leftTheTab = true;
        });

        jQuery(window).focus(function () {
            if (leftTheTab) {
                scoresheet.getById(matchId, function (returnSheet) {
                    if (returnSheet) {
                        $scope.scoresheet = returnSheet;

                        webSocketConnection = websocket.getWebsocket(matchId);
                        webSocketConnection.onmessage = function (event) {
                            $scope.scoresheet = JSON.parse(event.data);
                            $scope.$apply();
                        };

                        if (!$scope.scoresheet.date.end) {
                            $scope.live = true;
                        }
                    }
                });
            }
        });
    }

    /**
     * Get Table of current League
     */
    var liveTable = function () {
        if ($scope.scoresheet.matchInfo.currentSeason) {
            league.getRanksBySeasonId($scope.scoresheet.matchInfo.currentSeason, function (ranks) {
                if (ranks.data) {
                    $scope.rankings = ranks.data;
                }
            });
        }
    };


    /**
     * get live matches for current gameday & league
     */
    var liveMatches = function () {
        var liveGameOptions = {
            season: $scope.scoresheet.matchInfo.currentSeason,
            gameday: $scope.scoresheet.gameday,
            league: $scope.scoresheet.league
        };
        scoresheet.getAllLiveGames(liveGameOptions, function (liveScoresheets) {
            $scope.liveScoresheets = liveScoresheets;
        });
    };


    /**
     * Google Maps Integration
     * http://angular-ui.github.io/angular-google-maps/#!/api/marker
     *
     */
    var getGoogleMaps = function () {
        $scope.map = {
            center: {
                latitude: parseFloat($scope.scoresheet.location.lat),
                longitude: parseFloat($scope.scoresheet.location.lng)
            },
            coords: {
                latitude: parseFloat($scope.scoresheet.location.lat),
                longitude: parseFloat($scope.scoresheet.location.lng)
            },
            zoom: 15,
            options: {
                fullscreenControl: false,
                mapTypeControl: false,
                streetViewControl: false
            },
            marker: {},
            events: {
                click: function (marker, eventName, args) {

                    $scope.map.marker.options = {
                        draggable: true,
                        labelContent: '<p><strong>' + $scope.scoresheet.location.name + '</strong></p>',
                        labelAnchor: "100 0",
                        labelClass: "marker-labels"
                    };
                }
            }
        };
    };

    var updateMetaTags = function () {
        var meta = {
            teams: $scope.scoresheet.team_home.name + ' vs ' + $scope.scoresheet.team_guest.name,
            date: new Date($scope.scoresheet.date.create).toDateString(),
            league: '',
            gameday: ' // Freundschaftsspiel',
            url: window.location.origin + '/live?match=' + $scope.scoresheet.matchid
        };

        if ($scope.scoresheet.gameType === 'cup') {
            meta.gameday = ' // Pokalspiel';
        }

        if ($scope.scoresheet.gameType === 'gameday') {
            meta.league = ' // ' + $scope.scoresheet.matchInfo.leagueName;
            meta.gameday = ' // ' + $scope.scoresheet.matchInfo.gamedayNumber + '. Spieltag';
        }

        var desc = meta.date + meta.gameday + meta.league + ' // ' + meta.teams;

        document.querySelector('meta[property$="title"]').setAttribute("content", meta.teams);
        document.querySelector('meta[property$="description"]').setAttribute("content", desc);
        document.querySelector('meta[property$="url"]').setAttribute("content", meta.url);
    };

    var matchId = scoresheet.getIdFromUrl();
    if (matchId) {
        scoresheet.getById(matchId, function (returnSheet) {
            if (returnSheet) {
                $scope.scoresheet = returnSheet;

                webSocketConnection = websocket.getWebsocket(matchId);
                webSocketConnection.onmessage = function (event) {
                    $scope.scoresheet = JSON.parse(event.data);
                    $scope.$apply();
                };

                if (!$scope.scoresheet.date.end) {
                    $scope.live = true;
                }

                liveTable();
                getGoogleMaps();
                updateMetaTags();

                var oneSignalData = {};
                oneSignalData['matchid_' + scoresheet.matchId] = 'kkl';
                OneSignal.push(["sendTags", oneSignalData]);

                if ($scope.scoresheet.gameType === 'gameday') {
                    liveMatches();
                }

            } else {
                app.internalRoute();
            }
        });
    } else {
        app.internalRoute();
    }


    // TODO: umzug in editor Service
    $scope.managerLogin = function (e) {
        e.preventDefault();

        if ($scope.code) {
            if ($scope.scoresheet.user.hash === md5($scope.code)) {

                scoresheet.getById(matchId, function (returnSheet) {
                    if (returnSheet) {
                        $scope.scoresheet = returnSheet;
                        $scope.scoresheet.user.report.push(editor.getReport());

                        scoresheet.setValidation(matchId, function () {
                            scoresheet.save($scope.scoresheet, function () {
                                if (!$scope.scoresheet.team_home.player || !$scope.scoresheet.team_guest.player) {
                                    app.internalRoute('step2');
                                } else {
                                    app.internalRoute('step3');
                                }
                            });
                        });

                    } else {
                        alertHandler('No Connection.');
                    }
                });

            } else {
                alertHandler('Fehlerhafter Code.');
            }
        } else {
            alertHandler('Das Eingabefeld ist leer ...');
        }
    };

    var alertHandler = function (text) {
        var $alertbox = jQuery('#alertbox');

        $alertbox.addClass('in');
        $alertbox.children().html(text);

        setTimeout(function () {
            $alertbox.removeClass('in');
        }, 1300);
    };
}]);