'use strict';

app.controller('adminController', ['$scope', 'app', 'admin', 'editor', 'scoresheet', 'loader', function ($scope, app, admin, editor, scoresheet, loader) {

    $scope.adminMail = '';

    /**
     * show admin panel
     * if adminAuth === true
     */
    admin.getAdminAuth(function (status) {
        if(parseFloat(status) === 200){
            jQuery('body').addClass('-admin');
            $scope.adminAuth = true;
        }
    });


    /**
     * click: removeGame
     */
    $scope.removeGame = function () {
        var matchId = scoresheet.getIdFromUrl();

        scoresheet.removeById(matchId, function (response) {
            if (response > 0) {
                app.internalRoute();
            } else {
                console.log('cannot remove', matchId, response)
            }
        });
    };


    /**
     * click: editGame
     */
    $scope.editGame = function () {
        var matchId = scoresheet.getIdFromUrl();

        admin.getAdminAuth(function () {
            scoresheet.getById(matchId, function (returnSheet) {
                if (returnSheet) {
                    $scope.scoresheet = returnSheet;
                    $scope.scoresheet.user.report.push(editor.getReport(true));

                    scoresheet.setValidation(matchId, function () {
                        scoresheet.save($scope.scoresheet, function () {
                            if (!$scope.scoresheet.team_home.player || !$scope.scoresheet.team_guest.player) {
                                app.internalRoute('step2');
                            } else {
                                app.internalRoute('step3');
                            }
                        });
                    });
                } else {
                    console.error('no scoresheet found', returnSheet);
                }
            })
        })
    };


    $scope.completeGame = function () {
        var matchId = scoresheet.getIdFromUrl();

        admin.getAdminAuth(function () {
            scoresheet.getById(matchId, function (returnSheet) {
                if (returnSheet) {
                    $scope.scoresheet = returnSheet;
                    $scope.scoresheet.user.report.push(editor.getReport(true));
                    $scope.scoresheet.date['end'] = new Date();

                    scoresheet.setValidation(matchId, function () {
                        scoresheet.save($scope.scoresheet, function () {
                            app.internalRoute('overview');
                        });
                    });
                } else {
                    console.error('no scoresheet found', returnSheet);
                }
            })
        })
    };

    /**
     * send scoresheet by mail
     */
    $scope.sendAdminMail = function () {
        loader.showFunnyText();

        admin.getAdminAuth(function () {
            $scope.scoresheet.user.report.push(editor.getReport(true));
            scoresheet.sendMail($scope.scoresheet, function (result) {
                loader.fadeOut();

                $scope.adminMail = '';
            });
        });
    }
}]);