'use strict';

app.controller('statisticController', ['$scope', 'scoresheet', 'statistics', 'loader', function ($scope, scoresheet, statistics, loader) {
    $scope.statistics = {};
    $scope.season = new Date().getFullYear();

    var getSeason = function (gameType, season) {
        loader.showText('Lade Statistiken zur Saison');
        $scope.season = season;

        if (!$scope.statistics[$scope.season]) {
            statistics.getAggregatedStats('gameday', $scope.season, function (response) {
                $scope.statistics['gameday'] = response;

                console.info('statistics', $scope.statistics['gameday']);

                loader.fadeOut();
            });
        }
    };
    getSeason('gameday', $scope.season);


    /**
     * toggle season
     * @param season
     */
    $scope.updateSeason = function (season) {
        getSeason('gameday', season);
    };
}]);