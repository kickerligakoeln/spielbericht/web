'use strict';

app.controller('step1Controller', ['$scope', 'app', 'scoresheet', 'editor', 'league', 'match', 'team', 'location', 'loader', function ($scope, app, scoresheet, editor, league, match, team, location, loader) {
    loader.showFunnyText();
    scoresheet.reset();
    $scope.readTermsConditions = false;
    $scope.scoresheet = {
        gameType: 'custom'
    };

    league.getActiveLeagues(function (response) {
        $scope.activeLeagues = response.data;
        loader.fadeOut();
    });

    /**
     * Define GameType
     * gameday || custom || cup
     */
    $scope.$watch("scoresheet.gameType", function () {
        $scope.leagueId = undefined;
        $scope.allMatches = undefined;
        $scope.scoresheet = {
            user: {},
            matchInfo: {},
            team_home: {},
            team_guest: {},
            gameType: $scope.scoresheet.gameType
        };

        if ($scope.scoresheet.gameType !== 'gameday') {
            $scope.scoresheet.league = 'custom';
            $scope.scoresheet.matchid = new Date().getTime();
        }
    });


    /**
     * find currentGamedayId in activeLeagues by leagueId
     * get all matches for currentGamedayId
     * store in $scope.scoresheet
     */
    $scope.chooseLeague = function () {
        loader.showText('Lade Daten zur Liga');

        angular.forEach($scope.activeLeagues, function (league) {
            if (league.id === parseInt($scope.leagueId)) {
                $scope.scoresheet.league = league.code;
                $scope.scoresheet.matchInfo['leagueCode'] = league.code;
                $scope.scoresheet.matchInfo['leagueName'] = league.name;
                $scope.scoresheet.matchInfo['currentSeason'] = league.currentSeason;
                $scope.scoresheet.matchInfo['gamedayId'] = league._embedded.currentseason.currentGameDay;
            }
        });


        match.getUpcomingMatchesByGamedayId($scope.scoresheet.matchInfo.gamedayId, function (matches) {
            if (matches.data) {
                $scope.allMatches = matches.data;
                $scope.scoresheet.gameday = matches.data[0]._embedded.gameDay.number;
                $scope.scoresheet.matchInfo['gamedayNumber'] = matches.data[0]._embedded.gameDay.number;
                loader.fadeOut();
            } else {
                loader.updateText('Es konnten keine Spiele gefunden werden.');
            }
        });
    };


    /**
     * find team data in matches by matchId
     * get captains mails by teamId
     */
    $scope.chooseMatch = function () {
        loader.showText('Lade Daten zur Begegnung');
        $scope.scoresheet.matchInfo['matchId'] = $scope.scoresheet.matchid; // todo: brauch ich das noch?

        angular.forEach($scope.allMatches, function (match) {
            if (parseInt($scope.scoresheet.matchid) === match.id) {
                $scope.scoresheet.team_home = {
                    id: match._embedded.homeTeam.id,
                    club_id: match._embedded.homeTeam.club_id,
                    short_name: match._embedded.homeTeam.short_name,
                    name: match._embedded.homeTeam.name
                };

                $scope.scoresheet.team_guest = {
                    id: match._embedded.awayTeam.id,
                    club_id: match._embedded.awayTeam.club_id,
                    short_name: match._embedded.awayTeam.short_name,
                    name: match._embedded.awayTeam.name
                };

                if (match._embedded.location) {
                    $scope.scoresheet.location = match._embedded.location;
                    $scope.$broadcast('angucomplete-alt:changeInput', 'gamedayLocation', $scope.scoresheet.location.title);
                }
            }
        });

        team.getDetails($scope.scoresheet.team_home.id, function (teamHomeDetails) {
            $scope.scoresheet.team_home['mail'] = teamHomeDetails.data.properties.captainEmail;
            $scope.scoresheet.team_home['captain'] = teamHomeDetails.data.properties.captain;

            team.getDetails($scope.scoresheet.team_guest.id, function (teamAwayDetails) {
                $scope.scoresheet.team_guest['mail'] = teamAwayDetails.data.properties.captainEmail;
                $scope.scoresheet.team_guest['captain'] = teamAwayDetails.data.properties.captain;
                loader.fadeOut();
            });
        });
    };


    /**
     * Use suggestion list for custom location
     */
    $scope.locationSuggestionList = [];
    location.getAll(function (locations) {
        $scope.locationSuggestionList = locations.data;
    });

    $scope.selectedLocation = function (resp) {
        if (resp) {
            if (resp.originalObject.title) {
                $scope.scoresheet.location = {
                    title: resp.originalObject.title,
                    lat: resp.originalObject.lat,
                    lng: resp.originalObject.lng
                };
            } else {
                $scope.scoresheet.location = {
                    title: resp.originalObject,
                    lat: undefined,
                    lng: undefined
                };
            }
        }
    };


    function setCommonOption() {
        $scope.scoresheet.date = {};
        $scope.scoresheet.date['create'] = new Date();

        $scope.scoresheet.matches = {};
        $scope.scoresheet.team_home.goals = 0;
        $scope.scoresheet.team_home.set = 0;
        $scope.scoresheet.team_guest.goals = 0;
        $scope.scoresheet.team_guest.set = 0;

        $scope.scoresheet.user = {
            hash: md5('kkl-' + $scope.scoresheet.matchid),
            report: [editor.getReport()]
        }
    }

    /**
     * Submit 'GAMEDAY' form
     *
     * @param isValid
     * @param e
     */
    $scope.submitForm = function (isValid, e) {
        e.preventDefault();

        if (isValid && $scope.readTermsConditions) {
            loader.showFunnyText();
            setCommonOption();

            scoresheet.setValidation($scope.scoresheet.matchid, function () {
                scoresheet.save($scope.scoresheet, function () {
                    app.internalRoute('step2');
                });
            });
        } else {
            //highlight missing fields
            angular.forEach($scope.spielbericht.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        }
    };


    /**
     * Submit 'CUSTOM' form
     *
     * @param isValid
     * @param e
     */
    $scope.submitCustomForm = function (isValid, e) {
        e.preventDefault();

        if (isValid && $scope.readTermsConditions) {
            loader.showFunnyText();
            setCommonOption();

            scoresheet.setValidation($scope.scoresheet.matchid, function () {
                scoresheet.save($scope.scoresheet, function () {
                    app.internalRoute('step2');
                });
            });
        } else {
            //highlight missing fields
            angular.forEach($scope.spielberichtCustom.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        }
    }
}]);
