'use strict';

app.controller('mainController', ['$scope', 'app', 'loader', '$timeout', function ($scope, app, loader, $timeout) {

    /**
     * Check for Environment
     *
     * @type {{isLocal: boolean}}
     */
    $scope.env = {
        isLocal: (location.hostname === 'localhost')
    };


    /**
     * Chart.js Configuration
     */
    $scope.labels = [];
    $scope.options = {
        animation: {
            animateScale: true
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    };
    $scope.override = {
        backgroundColor: ['#ff0000', '#b8b8b8'],
        hoverBackgroundColor: ['#ff0000', '#b8b8b8'],
        borderColor: 'red',
        borderWidth: 0
    };


    /**
     * Google Maps integration
     */
    $scope.map = {
        center: {
            latitude: 45,
            longitude: -73
        },
        zoom: 8
    };


    /**
     * Slick Slider Configuration
     */
    $scope.sliderNavigation = function (slick, index) {
        var title = slick.$slides[index].children[0].children[0].dataset.title;
        return '<button type="button" data-role="none">' + title + '</button>';
    };


    /**
     * TODO: deprecated?
     * file download overlay
     */
    var appCache = window.applicationCache;
    if (appCache) {
        appCache.addEventListener('progress', function (event) {
            loader.fadeIn();
            loader.updateText("Download: " + event.loaded + " / " + event.total);
        }, false);
        appCache.addEventListener('updateready', loader.fadeOut, false);
        appCache.addEventListener('noupdate', loader.fadeOut, false);
        appCache.addEventListener('cached', loader.fadeOut, false);
    }


    /**
     * return class if winner exist
     *
     * @param setIndex
     * @param matchIndex
     * @returns {string}
     */
    $scope.getWinner = function (matchIndex, setIndex) {
        var winner = "";

        if (this.scoresheet.winner) {
            if (this.scoresheet.winner.match === matchIndex && this.scoresheet.winner.set === setIndex) {
                winner = "winner";
            }
        }

        return winner;
    };

    $scope.getCurrentHomePlayers = function () {
        var game = $scope.getActiveGame(this.scoresheet);
        if (game === null) return "";
        return game.player.home_a + " / " + game.player.home_b;
    };

    $scope.getCurrentAwayPlayers = function () {
        var game = $scope.getActiveGame(this.scoresheet);
        if (game === null) return "";
        return game.player.guest_a + " / " + game.player.guest_b;
    };

    $scope.getCurrentHomeGoals = function () {
        var game = $scope.getActiveGame(this.scoresheet);
        if (game === null) return 0;
        // we need to check if we are still playing the first set
        if ((game.score[0].home < 5 && game.score[0].guest < 5)) {
            return game.score[0].home;
        } else {
            return game.score[1].home;
        }
    };

    $scope.getCurrentAwayGoals = function () {
        var game = $scope.getActiveGame(this.scoresheet);
        if (game === null) return 0;
        // we need to check if we are still playing the first set
        if ((game.score[0].home < 5 && game.score[0].guest < 5)) {
            return game.score[0].guest;
        } else {
            return game.score[1].guest;
        }
    };

    $scope.getActiveGame = function (sheet) {
        // walk matches from last to first, finding first match with uncomplete score, but with a score
        // this is still wront for "vorgezogene" games because we would always be stuck at i.e. game 5 until
        if (typeof sheet === "undefined") return null;
        for (var i = sheet.matches.length; i-- > 0;) {
            var game = sheet.matches[i];
            if (game.score[0].home > 0 || game.score[0].guest > 0 || game.score[1].home > 0 || game.score[1].guest > 0) {
                // we found a game that has a score set, now check if this game is finished alreay
                if ((game.score[0].home < 5 && game.score[0].guest < 5) || (game.score[1].home < 5 && game.score[1].guest < 5)) {
                    // this game is not finished, return the game
                    return game;
                }
            }
        }
        // arriving here means we did not find a game that has either first or second set incomplete, now we need to
        // walk the other direction and return the first game with no score at all, this is still not accurate because
        // this means that we show the wrong game for "vorgezogene" games until a goal is scored...need to know "activeMatch"
        // from scoresheet
        for (var i = 0; i < sheet.matches.length; i++) {
            var game = sheet.matches[i];
            // its possible that we already played all games, return the last game then
            if (i === (sheet.matches.length - 1)) {
                return game;
            }
            if (game.score[0].home > 0) continue;
            if (game.score[0].guest > 0) continue;
            if (game.score[1].home > 0) continue;
            if (game.score[1].guest > 0) continue;
            return game;
        }
        // this should abolutey never happen...
        return null;
    };

    /**
     *
     * Share Facebook / Twitter
     *
     * @param type
     * @returns {boolean}
     */
    $scope.share = function (type) {
        var config = {
            url: window.location.href,
            facebookAppId: '1443958345634586',
            text: 'Kölner Kickerlga | Digitaler Spielberichtsbogen'
        };

        var baseUrl = '';
        var shareUrl = '';

        if (this.scoresheet) {
            config.text = this.scoresheet.team_home.name + ' vs ' + this.scoresheet.team_guest.name;
        }

        switch (type) {
            case 'facebook':
                baseUrl = "https://www.facebook.com/dialog/share";
                shareUrl = baseUrl + "?app_id=" + config.facebookAppId + "&display=popup&href=" + encodeURIComponent(config.url);
                break;

            case 'twitter':
                baseUrl = "http://twitter.com/share";
                shareUrl = baseUrl + "?text=" + encodeURIComponent(config.text) + "&url=" + encodeURIComponent(config.url);
                break;

            case 'messanger':
                baseUrl = 'fb-messenger://share';
                shareUrl = baseUrl + '?link=' + encodeURIComponent(config.url) + '&app_id=' + config.facebookAppId;
                break;

            case 'whatsapp':
                baseUrl = "whatsapp://send";
                shareUrl = baseUrl + "?text=" + encodeURIComponent(config.text) + "&url=" + encodeURIComponent(config.url);
                break;

            case 'telegram':
                baseUrl = 'https://telegram.me/share';
                shareUrl = baseUrl + '/url?url=' + encodeURIComponent(config.url) + '&text=' + encodeURIComponent(config.text);
                break;

            default:
                return false;
        }

        window.open(shareUrl, type, 'width=575,height=400');
    };


    /**
     *
     */
    $scope.copyLinkToClipboard = function () {
        // Create a dummy input to copy the string array inside it
        var linkElement = document.createElement("input");
        document.body.appendChild(linkElement);
        linkElement.setAttribute("id", "copy-link");
        document.getElementById("copy-link").value = window.location.href;

        linkElement.select();
        document.execCommand("copy");
        document.body.removeChild(linkElement);

        $scope.notification.trigger('Link kopiert!');
    };


    /**
     *
     * @type {{trigger: trigger}}
     */
    $scope.notification = {
        trigger: function (text) {
            $scope.notification.text = text;
            $scope.notification.show = true;

            $timeout(function () {
                $scope.notification.show = false;
            }, 1400);
        }
    };


    /**
     *
     * Subscribe for OneSignal Push Notification
     */
    $scope.subscribePushNotification = function () {
        OneSignal.push(function () {
            // OneSignal.registerForPushNotifications();
            OneSignal.showHttpPermissionRequest();
        });
    }
}]);