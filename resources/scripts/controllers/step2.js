'use strict';

app.controller('step2Controller', ['$scope', 'app', 'scoresheet', '$timeout', 'loader', function ($scope, app, scoresheet, $timeout, loader) {
    $scope.AMOUNT_MIN_PLAYER = 3;
    $scope.AMOUNT_MAX_PLAYER = 15;

    $scope.scoresheet = scoresheet.get();
    if (!$scope.scoresheet || !$scope.scoresheet.matchid) {
        app.internalRoute();
    }

    /**
     * Init player length
     * add more player
     */
    if (!$scope.scoresheet.team_home.player) {
        $scope.scoresheet.team_home.player = [];
        $scope.scoresheet.team_guest.player = [];
        for (var i = 0; i < $scope.AMOUNT_MIN_PLAYER; i++) {
            $scope.scoresheet.team_home.player.push({});
            $scope.scoresheet.team_guest.player.push({});
        }
    }

    $scope.addPlayer = function (type) {
        if (type === 'home') {
            if ($scope.scoresheet.team_home.player.length < $scope.AMOUNT_MAX_PLAYER) {
                $scope.scoresheet.team_home.player.push({});
            }
        } else {
            if ($scope.scoresheet.team_guest.player.length < $scope.AMOUNT_MAX_PLAYER) {
                $scope.scoresheet.team_guest.player.push({});
            }
        }
    };


    /**
     *
     * @param a
     */
    var shuffle = function shuffle(a) {
        var j, x, i;
        for (i = a.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    };

    /**
     *
     */
    var removeUnusedPlayers = function () {
        var i;
        for (i = 0; i < $scope.scoresheet.team_home.player.length; i++) {
            if (!$scope.scoresheet.team_home.player[i].name) {
                $scope.scoresheet.team_home.player.splice(i, 1);
                i--;
            }
        }
        for (i = 0; i < $scope.scoresheet.team_guest.player.length; i++) {
            if (!$scope.scoresheet.team_guest.player[i].name) {
                $scope.scoresheet.team_guest.player.splice(i, 1);
                i--;
            }
        }
    };

    /**
     * actually start to draw our stuff!
     * create an array of 10 games using a random distribution
     * create copies of our arrays
     */
    var draw = function () {
        loader.showFunnyText();
        $timeout(function () {
            var homePlayersCopy = [];
            var guestPlayersCopy = [];
            var matches = [];

            var createCopy = function () {
                if (homePlayersCopy.length === 0) {
                    homePlayersCopy = $scope.scoresheet.team_home.player.slice();
                    shuffle(homePlayersCopy);
                }
                if (guestPlayersCopy.length === 0) {
                    guestPlayersCopy = $scope.scoresheet.team_guest.player.slice();
                    shuffle(guestPlayersCopy);
                }
            };

            var tempName = '';
            for (var i = 0; i < 10; i++) {
                var player = {};
                createCopy(); //in case our stack is empty
                player.home_a = homePlayersCopy.pop().name;
                player.guest_a = guestPlayersCopy.pop().name;
                createCopy(); //in case our stack is empty
                var homeB = homePlayersCopy.pop().name;
                if (homeB === player.home_a) {
                    //we don't want this!
                    tempName = homeB;
                    homeB = homePlayersCopy.pop().name;
                    homePlayersCopy.push({
                        name: tempName
                    })
                }
                player.home_b = homeB;
                var guestB = guestPlayersCopy.pop().name;
                if (guestB === player.guest_a) {
                    //we don't want this!
                    tempName = guestB;
                    guestB = guestPlayersCopy.pop().name;
                    guestPlayersCopy.push({
                        name: tempName
                    });
                }
                player.guest_b = guestB;
                matches.push({
                    player: player,
                    score: [
                        {home: 0, guest: 0},
                        {home: 0, guest: 0}
                    ]
                });
            }

            $scope.scoresheet.matches = matches;
            $scope.$apply();
            loader.fadeOut();

            $("html, body").animate({
                scrollTop: $(document).height()
            }, 1000);
        }, 2000);
    };

    /**
     * generate empty matches for
     * custom drawing
     */
    var generateEmptyMatches = function () {
        $scope.scoresheet.matches = [];
        for (var i = 0; i < 10; i++) {
            $scope.scoresheet.matches.push({
                player: {
                    home_a: '',
                    home_b: '',
                    guest_a: '',
                    guest_b: ''
                },
                score: [
                    {home: 0, guest: 0},
                    {home: 0, guest: 0}
                ]
            });
        }
    };

    /**
     * @param type
     * @param valid
     */
    $scope.submitDrawingForm = function (type, valid) {
        if (valid) {
            $scope.scoresheet.drawingType = type;
            removeUnusedPlayers();
            if (type === 'auto') {
                draw();
            } else {
                generateEmptyMatches();
            }
        } else {
            angular.forEach($scope.teams.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        }
    };

    /**
     * send auto drawed schedule
     * @param event
     */
    $scope.sendAutoForm = function (event) {
        event.preventDefault();
        scoresheet.save($scope.scoresheet, function () {
            app.internalRoute('step3');
        });
    };

    /**
     * Send custom drawed schedule
     * @param event
     * @param valid
     */
    $scope.sendCustomForm = function (event, valid) {
        event.preventDefault();
        if (valid) {
            scoresheet.save($scope.scoresheet, function () {
                app.internalRoute('step3');
            });
        } else {
            angular.forEach($scope.customForm.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        }
    }
}]);