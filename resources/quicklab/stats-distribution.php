<section class="container" data-title="statistic-distribution">
    <div class="statistic-distribution">
        <div class="head">
            <span>Home</span>
            <span>Guest</span>
        </div>

        <div class="tile">
            <h3>Tore</h3>
            <div class="value">
                <div><span style="width:40%;">3021</span></div>
                <div><span style="width:73%;">3004</span></div>
            </div>
            <div class="avarage">
                <div><span style="width:35%;">120</span></div>
                <div><span style="width:44%;">77</span></div>
            </div>
        </div>
    </div>
</section>