<?php include('resources/views/base/doctype.php'); ?>
<head>
    <?php include('resources/views/base/head.php'); ?>
</head>
<body class="quicklab" ng-controller="mainController">

<header class="container">
    <div>
        Quicklab
        <small>Styleguide zum digitalen Spielberichtsbogen</small>
    </div>
</header>

<section class="container">
    <h1>Headline 1</h1>
    <h2>Headline 2</h2>
    <h3>Headline 3</h3>
    <h4>Headline 4</h4>

    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
        dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
        clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
        consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
        diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
        takimata sanctus est Lorem ipsum dolor sit amet.</p>
    <p>
        <a href="#">Text Link</a>
    </p>
</section>

<?php include('resources/quicklab/colors.php'); ?>
<?php include('resources/quicklab/controls.php'); ?>
<?php include('resources/quicklab/forms.php'); ?>

<section class="container -dark" data-title="scoreboard">
    <div class="scoreboard">
        <span class="goalcount_home">12</span>
        <span class="count_eq">:</span>
        <span class="goalcount_guest">3</span>
    </div>
</section>

<section class="container -dark" data-title="counterparty">
    <div class="counterparty">
        TFC Hammondbar '05 <small>vs</small> Rakete Kalk
    </div>
</section>

<?php include('resources/quicklab/collapse.php'); ?>
<?php include('resources/quicklab/hint.php'); ?>
<?php include('resources/quicklab/placeholder.php'); ?>

<?php include('resources/quicklab/stats-display.php'); ?>
<?php include('resources/quicklab/stats-distribution.php'); ?>
</body>
</html>