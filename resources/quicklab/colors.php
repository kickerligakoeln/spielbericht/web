<section class="container" data-title="colors">
    <ul class="colors" data-grid="3">
        <li style="background-color: #f00;">$color_1 <strong>#f00</strong></li>
        <li style="background-color: #151515;">$color_2 <strong>#151515</strong></li>
        <li style="background-color: #313131;">$color-paragraph <strong>#313131</strong></li>
    </ul>

    <ul class="colors" data-grid="4">
        <li style="background-color: #ededed; color: #313131;">$color_grey <strong>#ededed</strong></li>
        <li style="background-color: #ccc; color: #313131;">$color_greydark <strong>#ccc</strong></li>
        <li style="background-color: #231f20;">$color_dark <strong>#231f20</strong></li>
        <li style="background-color: #151515;">$color_batdark <strong>#151515</strong></li>
    </ul>

    <ul class="colors" data-grid="3">
        <li style="background-color: #c00;">$color_alert <strong>#c00</strong></li>
        <li style="background-color: #00CC3A;">$color_success <strong>#00CC3A</strong></li>
        <li style="background-color: #0F6499;">$color_hint <strong>#0F6499</strong></li>
    </ul>
</section>