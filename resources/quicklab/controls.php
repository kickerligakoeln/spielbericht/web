<section class="container" data-title="controls">
    <div data-grid="3">
        <div>
            <button class="btn-kkl">.btn-kkl</button>
        </div>
        <div>
            <button class="btn-default">.btn-default</button>
        </div>
        <div>
            <button class="btn-addplayer" data-icon="user-plus">.btn-addplayer</button>
        </div>
    </div>

    <div data-grid="3">
        <div>
            <button class="btn-kkl" data-icon="envelope">.btn-kkl</button>
        </div>

        <div>
            <a href="#" class="btn-default">
                Spiel wieder aufnehmen?
                <small>Stand: 12.03.2017 20:15Uhr</small>
            </a>
        </div>
        <div>
            <button class="btn-kkl" data-icon="envelope" disabled="disabled">:disabled</button>
        </div>
    </div>
</section>