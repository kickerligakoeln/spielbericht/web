<section class="container" data-title="hint">
    <div class="hint-static" data-icon="hint">
        <p>
            Du bist <strong>Spielleiter</strong> dieses Spiels. Nur du kannst dieses Spiel von diesem Gerät
            bearbeiten!
            Um diese Begegnung auf einem anderem Gerät bearbeiten zu können,
            gib folgenden Code auf der Live Seite zu diesem Spiel ein: <strong><em>xyz</em></strong>
        </p>

        <p>
            <strong>Wichtig!</strong><br/>
            Um Konflikte zu vermeiden, melde dich als Spielleiter von dieser Begegnung ab.
            <a href="#">Ausloggen</a>
        </p>
    </div>
</section>
