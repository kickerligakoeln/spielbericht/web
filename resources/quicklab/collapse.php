<section class="container" data-title="collapse">
    <div class="collapse-item">
        <input type="radio" id="collapse-1" name="collapse" checked/>
        <label for="collapse-1" data-count="02">4. Spieltag</label>
        <div class="collapse-content">

            <ul class="list-overview">
                <li>
                    <span class="league">2a</span>
                    <span class="date">17.05.</span>
                    <span class="match">
                        <a href="#" data-title="">
                            <strong>Balla Balla Ehrenfeld</strong> vs
                            <strong>Die Hand Gottes Grüne Wiese</strong>
                            (12 : 8)
                        </a>
                    </span>
                </li>
                <li>
                    <span class="league">1</span>
                    <span class="date">06.05.</span>
                    <span class="match">
                        <a href="#" data-title="">
                            <strong>Die Zerstörer</strong> vs
                            <strong>Lokomotive Kölschbar</strong>
                            (10 : 10)
                        </a>
                    </span>
                </li>
            </ul>

        </div>
    </div>

    <div class="collapse-item">
        <input type="radio" id="collapse-2" name="collapse"/>
        <label for="collapse-2" data-count="12">3. Spieltag</label>
        <div class="collapse-content">
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                et dolore
                magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                clita kasd
                gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
                consetetur sadipscing
                elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                voluptua. At vero
                eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                Lorem ipsum
                dolor sit amet.</p>
        </div>
    </div>
</section>