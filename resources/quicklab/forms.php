<section class="container" data-title="forms">
    <div data-grid="4">
        <div>
            <input type="radio" id="radio-1" name="radio"/>
            <label for="radio-1">Click mich</label>
        </div>

        <div>
            <select required size="1" class="select-form score">
                <option value="666">666</option>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>

        <div>
            <div class="select-group">
                <select name="formLiga" size="1" class="select-form" id="formLiga" required>
                    <option disabled selected></option>
                    <option value="koeln1">1. Liga</option>
                    <option value="koeln2a">2. Liga A</option>
                    <option value="koeln2b">2. Liga B</option>
                    <option value="koeln3a">3. Liga A</option>
                    <option value="koeln3b">3. Liga B</option>
                    <option value="koeln4a">4. Liga A</option>
                    <option value="koeln4b">4. Liga B</option>
                    <option value="koeln4c">4. Liga C</option>
                </select>
                <label data-icon="angle-down" for="formLiga"></label>
            </div>
        </div>

        <div></div>

        <div>
            <input type="text" value="" class="input-form" id="formMailHome"
                   name="formMailHome" placeholder="Lorem ipsum dolor">
        </div>

        <div>
            <input type="text" value="" class="input-form" id="formMailHome"
                   name="formMailHome" placeholder="Lorem ipsum dolor" disabled>
        </div>
    </div>
</section>