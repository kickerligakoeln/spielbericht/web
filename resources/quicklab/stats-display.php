<section class="container" data-title="statistic-display">
    <div data-grid="4">

        <div class="statistic-display">
            <div class="title">Tore</div>
            <div class="value">6083</div>
            <div class="avarage">152,075</div>
        </div>

        <div class="statistic-display">
            <div class="title">Sätze</div>
            <div class="value">800</div>
            <div class="avarage">20</div>
        </div>

        <div class="statistic-display">
            <div class="title">Spieler</div>
            <div class="value">525</div>
            <div class="avarage">13,125</div>
        </div>

        <div class="statistic-display">
            <div class="title">Spieldauer</div>
            <div class="value">14:53:03</div>
            <div class="avarage">01:25:66</div>
        </div>

    </div>
</section>