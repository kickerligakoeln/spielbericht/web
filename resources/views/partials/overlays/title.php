<div class="matchtitle" fixed-matchtitle="fixed">
    <div class="matchtitle-wrapper">
        <div class="container">
            <div class="matchtitle-inner" style="color: white;">
                {{scoresheet.team_home.name}} <small style="margin: 0px 5px 0px 5px;">vs</small> {{scoresheet.team_guest.name}}
            </div>
        </div>
    </div>

    <div class="matchtitle--spacer"></div>
</div>
