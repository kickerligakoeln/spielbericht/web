<div class="scoreboard-wrapper ng-cloak" ng-show="scoresheet.team_home">
    <div class="goalcount text-center">
        <p>Torverhältnis</p>

        <div class="scoreboard">
            <span class="goalcount_home">{{scoresheet.team_home.goals}}</span>
            <span class="count_eq">:</span>
            <span class="goalcount_guest">{{scoresheet.team_guest.goals}}</span>
        </div>
    </div>
    <div class="setcount text-center">
        <p>Satzverhältnis</p>

        <div class="scoreboard">
            <span class="setcount_home"
                  ng-class="{winner: scoresheet.team_home.set > 10}">{{scoresheet.team_home.set}}</span>
            <span class="count_eq">:</span>
            <span class="setcount_guest"
                  ng-class="{winner: scoresheet.team_guest.set > 10}">{{scoresheet.team_guest.set}}</span>
        </div>
    </div>
</div>