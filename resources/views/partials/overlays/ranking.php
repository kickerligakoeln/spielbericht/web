<div class="livetable-content">
    <div class="container">

        <h2>Tabelle</h2>

        <table class="table table-striped text-center">
            <thead>
            <tr>
                <th width="3%" class="index">#</th>
                <th class="text-left">Team</th>
                <th class="text-center">Spiele</th>
                <th class="text-center visible-xs">g/u/v</th>
                <th class="text-center hidden-xs">g.</th>
                <th class="text-center hidden-xs">u.</th>
                <th class="text-center hidden-xs">v.</th>
                <th class="text-center set_rate">
                    <span class="visible-xs">diff</span>
                    <span class="hidden-xs">Satzdiff.</span>
                </th>
                <th class="text-center set hidden-xs">Sätze</th>
                <th class="text-center goal_rate hidden-xs">Tordiff.</th>
                <th class="text-center goals hidden-xs">Tore</th>
                <th class="text-center points">Punkte</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="team in rankings">
                <td width="3%" class="index">
                    <label for="">{{team.position}}.</label>
                </td>
                <td class="text-left">{{team._embedded.team.name}}</a></td>
                <td>{{team.games}}</td>
                <td class="visible-xs">{{team.wins}}/{{team.draws}}/{{team.losses}}</td>
                <td class="hidden-xs">{{team.wins}}</td>
                <td class="hidden-xs">{{team.draws}}</td>
                <td class="hidden-xs">{{team.losses}}</td>
                <td class="">{{team.gameDiff}}</td>
                <td class="hidden-xs">{{team.gamesFor}}:{{team.gamesAgainst}}</td>
                <td class="hidden-xs">{{team.goalDiff}}</td>
                <td class="hidden-xs">{{team.goalsFor}}:{{team.goalsAgainst}}</td>
                <td><strong>{{team.score}}</strong></td>
            </tr>

            <tr ng-show="!(rankings).length">
                <td colspan="12">
                    <p><em>Leider liegen keine Daten vor.</em></p>
                </td>
            </tr>
            </tbody>
        </table>

    </div>
</div>