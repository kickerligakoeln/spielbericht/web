<div class="container">
    <h2>Ergebnisse</h2>
    <table border="0" width="100%" class="table table-striped score">
        <thead>
        <tr>
            <th class="text-center" width="4%"></th>
            <th class="text-center" width="20%"><strong>Heim</strong></th>
            <th class="text-center" width="4%"></th>
            <th class="text-center" width="20%"><strong>Gast</strong></th>
            <th class="text-center" width="25%" colspan="2"><strong>Satz 1</strong></th>
            <th class="text-center" width="25%" colspan="2"><strong>Satz 2</strong></th>
        </tr>
        </thead>

        <tr ng-repeat="match in scoresheet.matches">
            <td width="3%" class="index">
                <label for="">{{$index + 1}}.</label>
            </td>
            <td class="team">
                {{match.player.home_a | friendlyPlayerName}}<br/>{{match.player.home_b | friendlyPlayerName}}
            </td>
            <td width="3%" class="team">
                <em>vs</em>
            </td>
            <td class="team">
                {{match.player.guest_a | friendlyPlayerName}}<br/>{{match.player.guest_b | friendlyPlayerName}}
            </td>
            <td width="13%" class="divider text-right">
                <span>{{match.score[0].home}}</span>
            </td>
            <td width="13%" class="divider text-left">
                <span>{{match.score[0].guest}}</span>
            </td>
            <td width="13%" class="divider text-right">
                <span>{{match.score[1].home}}</span>
            </td>
            <td width="13%" class="divider text-left">
                <span>{{match.score[1].guest}}</span>
            </td>
        </tr>
    </table>
</div>