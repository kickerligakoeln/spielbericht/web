<div class="container">
    <div class="game-trend--rank">
        <h2>Spielverlauf</h2>

        <div>
            <div class="game-trend-label">
                <strong>Heim</strong>
                vs
                <strong>Gast</strong>
            </div>
		        <ul>
                <li ng-repeat="match in scoresheet.matches">
                    <span ng-class="getWinner($index+1, 1)"
                          ng-attr-data-label="{{match.score[0] | trendLabel }}"
                          ng-attr-data-trend="{{match.score[0] | trend}}"></span>
                    <span ng-class="getWinner($index+1, 2)"
                          ng-attr-data-label="{{match.score[1] | trendLabel }}"
                          ng-attr-data-trend="{{match.score[1] | trend}}"></span>
                </li>
            </ul>
				</div>
    </div>
</div>
