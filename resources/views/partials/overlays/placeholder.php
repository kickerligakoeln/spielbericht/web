<?php
$text = $_GET['text'];
?>

<div class="matchtitle" fixed-matchtitle="fixed">
    <div class="matchtitle-wrapper">
        <div class="container">
            <div class="matchtitle-inner" style="color: white;">
							<div class="logo">
     					   <a href="<?php echo REPORT_PATH; ?>">
     					       <img height="45px" src="<?php echo REPORT_PATH; ?>/public/images/KoelnerKickerliga_Logo_4c_weiss.svg" alt="Kölner Kickerliga | Digitaler Spielberichtsbogen"/>
     					   </a>
							</div>
							<div style="text-rendering: geometricPrecision; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-family: 'notoserif-italic',Georgia,serif; font-weight: normal; font-style: normal; line-height: 1.2; letter-spacing: 1px; color: #fff; font-size: 16px; line-height: 1; text-align: center;" ><?php echo $text; ?></div>
            </div>
        </div>
    </div>

    <div class="matchtitle--spacer"></div>
</div>

