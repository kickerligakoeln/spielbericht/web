<?php
$swap = $_GET['swap'];
$hideScore = $_GET['hideScore'];
?>
<div class="matchtitle">
    <div class="matchtitle-wrapper">
        <div class="container">
            <div class="matchtitle-inner">

                <div class="counterparty">
                    <?php if($swap): ?>
                        {{getCurrentAwayPlayers()}}
                    <?php else: ?>
                        {{getCurrentHomePlayers()}}
                    <?php endif; ?>
                </div>
  
								<div class="scoreboard">
								<?php if(!$hideScore): ?>
									<?php if($swap): ?>
                	    <span class="goalcount_home">{{getCurrentAwayGoals()}}</span>
                	    <span class="count_eq">:</span>
                	    <span class="goalcount_guest">{{getCurrentHomeGoals()}}</span>
                	<?php else: ?>
                	    <span class="goalcount_home">{{getCurrentHomeGoals()}}</span>
                	    <span class="count_eq">:</span>
                	    <span class="goalcount_guest">{{getCurrentAwayGoals()}}</span>
                	<?php endif; ?>
								<?php else: ?>
                	<span style="color: white; margin: 0px 300px 0px 300px;">&nbsp;</span>
								<?php endif; ?>
                </div>
              
                <div class="counterparty">
                    <?php if($swap): ?>
                        {{getCurrentHomePlayers()}}
                    <?php else: ?>
                        {{getCurrentAwayPlayers()}}
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>

    <div class="matchtitle--spacer"></div>
</div>
