<header class="topbar">

    <div class="logo">
        <a href="<?php echo REPORT_PATH; ?>">
            <img height="45px" src="<?php echo REPORT_PATH; ?>/public/images/KoelnerKickerliga_Logo_4c_weiss.svg"
                 alt="Kölner Kickerliga | Digitaler Spielberichtsbogen"/>
        </a>
    </div>

    <?php include('resources/views/partials/overlays/scoreboard.php'); ?>

</header>

<section class="topbar--spacer"></section>