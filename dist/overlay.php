<?php
$overlay = basename($_GET['snippet']);
$reload = $_GET['reload'];
?>
<?php include('resources/views/base/doctype.php'); ?>
<head>
    <?php include('resources/views/base/head.php'); ?>
    <?php if(is_numeric($reload)): ?>
    <script type="text/JavaScript">
        setTimeout("location.reload();",<?php echo ($reload*1000) ?>);
    </script>
    <?php endif;?>
</head>
<body class="uebersicht overlay" ng-controller="mainController">

<div ng-controller="liveController" class="ng-hide" ng-show="scoresheet" style="background: rgba(237, 237, 237, 0.9);">
    <?php include('resources/views/overlays/' . $overlay . '.php'); ?>

    <script type="text/javascript">
        var app_settings = {
            report_path: '<?php echo REPORT_PATH; ?>'
        }
    </script>

    <?php include('resources/views/atoms/alertbox.php'); ?>
    <?php include('resources/views/atoms/spinner.php'); ?>
    <?php include('resources/views/base/scripts.php'); ?>
</div>
</body>
</html>
