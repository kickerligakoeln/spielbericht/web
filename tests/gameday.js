var config = require('../inc/config-tests');
var consoleHelper = require('helper/consoleHelper');

casper.test.begin('Test Gameday', function (test) {
    casper.start(config.report_path, function () {
        test.assertExists('.game a.btn-kkl', 'Button "Neues Spiel starten!" exists');

        console.log('-- [click "Neues Spiel starten!]');
        this.click('.game a.btn-kkl');
    });


    /**
     * /step1.php
     */
    casper.waitForUrl(/step1/, function () {
        test.assertUrlMatch(/step1/, 'New page is located: ' + this.getCurrentUrl());

        this.waitFor(
            function () {
                return this.thenEvaluate(consoleHelper.selectLeague);
            },
            function () {
                this.wait(1000)
                    .then(function () {
                        test.assertVisible('form#spielbericht #matchid', 'Select League');
                    })
                    .waitFor(
                        function () {
                            return this.thenEvaluate(consoleHelper.selectMatch);
                        },
                        function () {
                            this.wait(1000)
                                .then(function () {
                                    test.assert((this.fetchText('#formMailHome') !== ''), 'Select first Match');

                                    this.fill('form#spielbericht', {
                                        'formMailHome': config.mailHome,
                                        'formMailGuest': config.mailGuest
                                    });
                                })
                                .then(function () {
                                    this.captureSelector(config.capture_path + '/step1.png', '#spielbericht');

                                    console.log('-- [click "Weiter zu Teamdetails"]');
                                    this.click('form#spielbericht #sendForm');
                                })
                        }
                    )
            })
    }, function (t, e) {
        console.error('step1 was not loaded', e);
    }, 10000);


    /**
     * /step2.php
     */
    casper.waitForUrl(/step2/, function () {
        test.assertUrlMatch(/step2/, 'New page is located: ' + this.getCurrentUrl());

        this.waitFor(
            function () {
                return this.thenEvaluate(consoleHelper.setPlayerName, config.playerNames);
            },
            function () {
                this.wait(1000)
                    .then(function () {
                        test.assert((this.fetchText('.input-form') !== ''), 'Set Teammates');
                        this.captureSelector(config.capture_path + '/step2-teams.png', '#content');

                        console.log('-- [click "Begegnung losen"]');
                        this.click('a.btn-kkl[data-icon="shuffle"]');
                    })
                    .waitUntilVisible('#generatedMatches', function () {
                        test.assertVisible('#generatedMatches', 'Successfully created matches');

                        this.captureSelector(config.capture_path + '/step2-matches.png', '#generatedMatches');

                        console.log('-- [click "Spielen!"]');
                        this.click('#generatedMatches ~ .section-form a.btn-kkl');
                    }, function () {
                    }, 10000)
            }
        )
    }, function (t, e) {
        console.error('step2 was not loaded', e);
    }, 10000);


    /**
     * /step3.php
     */
    casper.waitForUrl(/step3/, function () {
        test.assertUrlMatch(/step3/, 'New page is located: ' + this.getCurrentUrl());

        this.waitFor(
            function () {
                return this.thenEvaluate(consoleHelper.setScores);
            },
            function () {
                this.wait(1000)
                    .then(function () {
                        test.assert((this.fetchText('.select-form') !== ''), 'Set Scores');
                        test.assertMatch(this.fetchText('.setcount_home'), /0/, 'Home won 0 sets');
                        test.assertMatch(this.fetchText('.setcount_guest'), /20/, 'Guest won 20 sets');
                        test.assertMatch(this.fetchText('.goalcount_home'), /20/, 'Home shot 20 goals');
                        test.assertMatch(this.fetchText('.goalcount_guest'), /100/, 'Guest shot 100 goals');
                    })
                    .then(function () {
                        this.captureSelector(config.capture_path + '/step3.png', '#scoreboard');

                        console.log('-- [click "Zusammenfassung!"]');
                        this.click('#checkScore');
                    })
            }
        )
    }, function (t, e) {
        console.error('step3 was not loaded', e);
    }, 10000);


    /**
     * /step4.php
     */
    casper.waitForUrl(/step4/, function () {
        test.assertUrlMatch(/step4/, 'New page is located: ' + this.getCurrentUrl());

        this.then(function () {
            this.captureSelector(config.capture_path + '/step4.png', 'body');

            console.log('-- [click "Spielbogen abschicken"]');
            this.click('a.btn-kkl');
        })
            .waitUntilVisible('div.container[ng-show="showSuccess"]', function () {
                test.assertVisible('div.container[ng-show="showSuccess"] h2', 'Show success Message');
            })
    }, function (t, e) {
        console.error('step3 was not loaded', e);
    }, 10000);


    /**
     * run!
     */
    casper.run(function () {
        console.log('---------------------');
        test.done();
    });
});