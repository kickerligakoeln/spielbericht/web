module.exports = {

    selectLeague: function () {
        var sel = document.querySelector('form#spielbericht #formLiga');
        sel.selectedIndex = 1;
        angular.element(document.getElementById('formLiga')).triggerHandler('change');

        return true;
    },

    selectMatch: function () {
        var sel = document.querySelector('form#spielbericht #matchid');
        sel.selectedIndex = 1;
        angular.element(document.getElementById('matchid')).triggerHandler('change');

        return true;
    },

    setPlayerName: function (players) {
        var input = document.querySelectorAll('form#teams .input-form');
        for (i = 0; i < input.length; i++) {
            $(input[i]).val(players[i]);
        }
        angular.element(document.getElementsByClassName('input-form')).triggerHandler('change');
    },

    setScores: function () {
        var sel = document.querySelectorAll('form#scoreboard .select-form:not(#suddenDeath)');
        for (i = 0; i < sel.length; i++) {
            if ((i % 2) === 0) {
                sel[i].selectedIndex = 2;
            } else {
                sel[i].selectedIndex = 5;
            }
        }
        angular.element(document.getElementsByClassName('select-form')).triggerHandler('change');
    }

};