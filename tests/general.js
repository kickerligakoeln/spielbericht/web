casper.test.begin('Überprüfung ob spiel.kickerligakoeln.de online ist', function (test) {
    casper.start();

    casper.thenOpen("http://spiel.kickerligakoeln.de/", function (response) {
        test.assertEquals(response.status, 200, "http://spiel.kickerligakoeln.de/");
    });

    casper.run(function () {
        test.done();
    });
});