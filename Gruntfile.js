module.exports = function (grunt) {

    require('time-grunt')(grunt);

    var sass = require('node-sass');
    var conf = {
        cssCwd: 'resources/scss',
        cssDist: 'dist/css',

        jsCwd: 'resources/scripts',
        jsDist: 'dist/scripts',
        npm: 'node_modules',

        fontsCwd: 'resources/fonts',
        fontsDist: 'dist/fonts',

        viewsCwd: 'resources/views',
        viewsDist: 'dist',

        imagesCwd: 'resources/images',
        imagesDist: 'dist/images',

        toolsCwd: 'tools/',

        env: grunt.file.readYAML('env.yml')
    };

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        /**
         * compile sass
         * https://www.npmjs.com/package/grunt-sass
         */
        sass: {
            dist: {
                options: {
                    implementation: sass,
                    outFile: null,
                    sourceMap: false,
                    sourceMapContents: true,
                    sourceMapRoot: '../../',
                    outputStyle: 'compressed'
                },

                files: [{
                    expand: true,
                    cwd: conf.cssCwd,
                    src: [
                        '*.scss'
                    ],
                    dest: conf.cssDist,
                    ext: '.min.css'
                }]
            }
        },

        postcss: {
            options: {
                map: {
                    inline: false,
                    annotation: conf.cssDist
                },
                processors: [
                    require('autoprefixer')({browsers: 'last 2 versions, safari 8'}), // add vendor prefixes
                    require('postcss-discard-comments')({removeAll: true}) // remove comments
                ]
            },
            dist: {
                src: conf.cssDist + '/*.css'
            }
        },

        /**
         * Combine Javascript
         * https://github.com/gruntjs/grunt-contrib-uglify
         */
        uglify: {
            libraries: {
                files: [{
                    src: [
                        conf.npm + "/angular/angular.js",
                        conf.npm + "/angular-locale/dist/angular-locale.js",
                        conf.npm + "/angucomplete-alt/angucomplete-alt.js",
                        conf.npm + "/jquery/dist/jquery.js",
                        conf.npm + "/bootstrap/dist/js/bootstrap.js",
                        conf.npm + "/slick-carousel/slick/slick.js",
                        conf.npm + "/angular-slick/dist/slick.js",
                        conf.npm + "/chart.js/dist/Chart.js",
                        conf.npm + "/angular-chart.js/dist/angular-chart.js",
                        conf.npm + "/lodash/lodash.js",
                        conf.npm + "/angular-simple-logger/dist/angular-simple-logger.js",
                        conf.npm + "/angular-google-maps/dist/angular-google-maps.js",
                        conf.npm + "/blueimp-md5/js/md5.min.js"
                    ],
                    dest: conf.jsDist + "/libraries.min.js"
                }]
            },

            services: {
                files: [{
                    src: conf.jsCwd + "/services/*.js",
                    dest: conf.jsDist + "/services.min.js"
                }]
            },

            filters: {
                files: [{
                    src: conf.jsCwd + "/filter/*.js",
                    dest: conf.jsDist + "/filter.min.js"
                }]
            },

            directives: {
                expand: true,
                files: [{
                    src: conf.jsCwd + "/directives/*.js",
                    dest: conf.jsDist + "/directives.min.js"
                }]
            },

            controllers: {
                expand: true,
                files: [{
                    src: conf.jsCwd + "/controllers/*.js",
                    dest: conf.jsDist + "/controller.min.js"
                }]
            },

            app: {
                expand: true,
                files: [{
                    src: conf.jsCwd + "/app.js",
                    dest: conf.jsDist + "/app.min.js"
                }]
            }

        },


        /**
         * Copy Files & Dependencies
         * https://github.com/gruntjs/grunt-contrib-copy
         */
        copy: {
            fonts: {
                files: [
                    {
                        expand: true,
                        flatten: false,
                        cwd: conf.fontsCwd,
                        src: ['*/**.otf', '*/*.eot', '*/**.svg', '*/**.ttf', '*/**.woff', '*/**.woff2'],
                        dest: conf.fontsDist
                    }
                ]
            },
            cssLib: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        cwd: conf.cssCwd,
                        src: ['vendor/**.css'],
                        dest: conf.cssDist
                    }
                ]
            },
            jsLib: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        cwd: conf.jsCwd,
                        src: ['lib/**.js'],
                        dest: conf.jsDist
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: conf.npm,
                        src: ['html5shiv/dist/html5shiv.js'],
                        dest: conf.jsDist
                    }
                ]
            },
            images: {
                files: [
                    {
                        expand: true,
                        flatten: false,
                        cwd: conf.imagesCwd,
                        src: ['*'],
                        dest: conf.imagesDist
                    }
                ]
            },
            htaccess: {
                files: [
                    {
                        expand: true,
                        flatten: false,
                        cwd: conf.toolsCwd,
                        src: ['.htaccess'],
                        dest: conf.viewsDist
                    }
                ]
            }
        },


        /**
         * replace text pattern in html-files
         * https://www.npmjs.com/package/grunt-replace
         */
        replace: {
            views: {
                options: {
                    patterns: [
                        {
                            // include timestamp in template (e.g. @@timestamp)
                            match: 'timestamp',
                            replacement: '<%= new Date() %>'
                        },
                        {
                            // include templates (e.g. @@inc=folder/filename)
                            match: /(@@inc=)\S+/g,
                            replacement: function (match, offset, string, source, target) {
                                var templateName = match.replace(/@@inc=/g, '');
                                return grunt.file.read(conf.viewsCwd + "/partials/" + templateName + ".html");
                            }
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        cwd: conf.viewsCwd,
                        src: ['**', '!partials/**'],
                        dest: conf.viewsDist
                    }
                ]
            },
            vars: {
                options: {
                    patterns: [
                        {
                            // include timestamp to avoid caching in template (e.g. @@cache)
                            match: 'cache',
                            replacement: '<%= Date.now() %>'
                        },
                        {
                            // include timestamp in template (e.g. @@year)
                            match: 'year',
                            replacement: '<%= new Date().getFullYear() %>'
                        },
                        {
                            match: /(@@\w+@@)/g,
                            replacement: function(match) {
                                return conf.env[match.replace(/@@/g, '')];
                            }
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        cwd: conf.viewsDist,
                        src: ['**.html'],
                        dest: conf.viewsDist
                    }
                ]
            },
        },


        /**
         *
         * Watch Task
         *
         */
        watch: {
            css: {
                files: conf.cssCwd + "/**/*.scss",
                tasks: ["sass", "postcss"],
                options: {
                    spawn: false
                }
            },

            services: {
                files: conf.jsCwd + "/services/*.js",
                tasks: ["uglify:services"],
                options: {
                    spawn: false
                }
            },
            filters: {
                files: conf.jsCwd + "/filter/*.js",
                tasks: ["uglify:filters"],
                options: {
                    spawn: false
                }
            },
            directives: {
                files: conf.jsCwd + "/directives/*.js",
                tasks: ["uglify:directives"],
                options: {
                    spawn: false
                }
            },
            controllers: {
                files: conf.jsCwd + "/controllers/*.js",
                tasks: ["uglify:controllers"],
                options: {
                    spawn: false
                }
            },

            views: {
                files: [
                    conf.viewsCwd + '/**/*.html'
                ],
                tasks: ['replace'],
                options: {
                    spawn: false
                }
            }
        }

    });


    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-replace");
    grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.registerTask("set_dev", "Set Config variables for dev tasks", function () {
        grunt.config.set("uglify.services.options.sourceMap", true);
        grunt.config.set("uglify.filters.options.sourceMap", true);
        grunt.config.set("uglify.controllers.options.sourceMap", true);
        grunt.config.set("uglify.app.options.sourceMap", true);
    });

    grunt.registerTask("set_prod", "Set Config variables for production tasks", function () {
        conf.env = grunt.file.readYAML('env.prod.yml')
    });

    // Default task(s).
    grunt.registerTask('default', ['sass', 'uglify', 'copy', 'replace']);
    grunt.registerTask('dev', ['set_dev', 'default', 'watch']);
    grunt.registerTask('prod', ['set_prod', 'default']);
};
