# Digitaler Spielberichtsbogen
**the web layer**

## Requirements
* [Docker](https://www.docker.com/) v2.0.0.2
* [Node](https://nodejs.org/en/) v9.9.0
* [Grunt](https://gruntjs.com/) v1.0.3

#### setup environment
```bash
$ cp docker/example.env .env
$ cp tools/example-env.yml env.yml # for local environment
$ cp tools/example-env.yml env.prod.yml # for production
```

#### build application
```bash
$ npm install
$ grunt [dev|prod]
```

#### start nginx
```bash
$ docker-compose up --build
```

find your web service here: http://localhost:8080/
